import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: () =>
        import(/* webpackChunkName: "HomePage"*/ './pages/HomePage.vue'),
      props: true,
    },
    {
      path: '/photos/:id',
      name: 'PhotoDetails',
      props: true,
      component: () =>
        import(
          /* webpackChunkName: "PhotoDetails"*/ './pages/PhotoDetails.vue'
        ),
    },
    {
      path: '/users/:username',
      name: 'UserPage',
      props: true,
      component: () =>
        import(/* webpackChunkName: "UserPage"*/ './pages/UserPage.vue'),
    },
    {
      path: '/users/:username/:page',
      name: 'UserPage',
      props: true,
      component: () =>
        import(/* webpackChunkName: "UserPage"*/ './pages/UserPage.vue'),
    },
  ],
})

export default router
