import { onMounted, reactive, toRefs, watch } from '@vue/composition-api'
import { fetchPhotosList } from '../services/unsplash.js'
import get from 'lodash.get'

export function usePhotosList(page = 1) {
  const data = reactive({
    loading: false,
    photosMock: [],
    numberOfPages: 1,
  })

  watch(page, () => {
    getPhotosList()
  })

  const getPhotosList = async () => {
    data.loading = true
    try {
      const result = await fetchPhotosList(page.value)
      data.photosMock = result.data
      data.numberOfPages =
        Math.ceil(
          get(result.headers, 'x-total') / get(result.headers, 'x-per-page')
        ) - 1
      console.log(data)
    } catch (error) {
      console.log(error)
    } finally {
      data.loading = false
    }
  }

  const dataRefs = toRefs(data)

  onMounted(() => {
    getPhotosList()
  })

  return {
    ...dataRefs,
  }
}
