import { onMounted, reactive, toRefs, watch } from '@vue/composition-api'
import { fetchPhotoDetails } from '../services/unsplash'
import get from 'lodash.get'

export function usePhotoDetails(id) {
  const data = reactive({
    loading: false,
    photoDetails: {},
    location: '',
    regularImage: '',
  })

  watch(id, () => {
    getPhotoDetails()
  })

  const getPhotoDetails = async () => {
    data.loading = true

    try {
      const result = await fetchPhotoDetails(id.value)
      data.photoDetails = result.data
      data.location = get(result.data, 'location.title', 'default value')
      data.regularImage = get(
        result.data,
        'urls.regular',
        'https://images.unsplash.com/photo-1593642634443-44adaa06623a?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjE1NzgxNH0'
      )
    } catch (error) {
      console.log(error)
    } finally {
      data.loading = false
    }
  }

  const dataRefs = toRefs(data)

  onMounted(() => {
    getPhotoDetails()
  })

  return {
    ...dataRefs,
  }
}
