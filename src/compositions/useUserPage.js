import { onMounted, reactive, toRefs, watch } from '@vue/composition-api'
import { fetchUserInfo, fetchUserPhotosList } from '../services/unsplash.js'
import get from 'lodash.get'

export function useUserPage(username, page = 1) {
  const data = reactive({
    loading: false,
    photosMock: [],
    userInfo: {},
    profileImageMedium: '',
  })

  watch(page, () => {
    getUserInfoAndPhotosList()
  })

  const getUserInfoAndPhotosList = async () => {
    data.loading = true
    try {
      const userInfo = await fetchUserInfo(username.value, page.value)
      const userPhotosList = await fetchUserPhotosList(
        username.value,
        page.value
      )
      data.userInfo = userInfo.data
      data.profileImageMedium = get(
        userInfo.data,
        'profile_image.medium',
        'https://images.unsplash.com/photo-1593642634443-44adaa06623a?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&ixid=eyJhcHBfaWQiOjE1NzgxNH0'
      )
      data.photosMock = userPhotosList.data
    } catch (error) {
      console.log(error)
    } finally {
      data.loading = false
    }
  }

  const dataRefs = toRefs(data)

  onMounted(() => {
    getUserInfoAndPhotosList()
  })

  return {
    ...dataRefs,
  }
}
