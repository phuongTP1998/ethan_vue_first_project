import axios from 'axios'

const UnsplashApiKey = process.env.VUE_APP_UNSPLASH_API_KEY || ''

const api = axios.create({
  baseURL: 'https://api.unsplash.com',
  headers: {
    'Accept-Version': 'v1',
    Authorization: `Client-ID ${UnsplashApiKey}`,
  },
})

export const fetchUserInfo = username => {
  return api.get(`users/${username}`)
}

export const fetchPhotosList = (page = 1) => {
  return api.get(`photos?page=${page}`)
}

export const fetchUserPhotosList = (username, page) => {
  return api.get(`users/${username}/photos?page=${page}`)
}

export const fetchPhotoDetails = id => {
  return api.get(`photos/${id}`)
}
